import io.gitlab.jfronny.scripts.*
import org.jetbrains.kotlin.gradle.dsl.JvmTarget

plugins {
    id("java")
    id("org.jetbrains.intellij.platform") version "2.2.1"
    kotlin("jvm") version "2.1.0"
    kotlin("plugin.serialization") version "2.1.0"
    id("jf.autoversion") version "1.6-SNAPSHOT"
    id("de.undercouch.download") version "5.6.0"
}

group = "io.gitlab.jfronny"

repositories {
    mavenCentral()

    intellijPlatform {
        defaultRepositories()
    }
}


dependencies {
    intellijPlatform {
        clion("251.14649.40") // https://plugins.jetbrains.com/docs/intellij/intellij-artifacts.html
    }
    implementation("io.ktor:ktor-client-core:3.0.3")
    implementation("io.ktor:ktor-client-java:3.0.3")
    implementation("io.ktor:ktor-client-auth:3.0.3")
    implementation("io.ktor:ktor-client-content-negotiation:3.0.3")
    implementation("io.ktor:ktor-serialization-kotlinx-json:3.0.3")

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.8.0")
}

tasks {
    val extraResources = layout.buildDirectory.dir("downloads/resources")
    val pdfJsVersion = "3.11.174"
    val downloadPdfViewerCss by registering(de.undercouch.gradle.tasks.download.Download::class) {
        src("https://cdnjs.cloudflare.com/ajax/libs/pdf.js/$pdfJsVersion/pdf_viewer.min.css")
        dest(extraResources.map { it.file("pdf.viewer.css") })
        overwrite(false)
    }
    val downloadPdfViewerJs by registering(de.undercouch.gradle.tasks.download.Download::class) {
        src("https://cdnjs.cloudflare.com/ajax/libs/pdf.js/$pdfJsVersion/pdf.min.js")
        dest(extraResources.map { it.file("pdf.mjs") })
        overwrite(false)
    }
    val downloadPdfWorkerJs by registering(de.undercouch.gradle.tasks.download.Download::class) {
        src("https://cdnjs.cloudflare.com/ajax/libs/pdf.js/$pdfJsVersion/pdf.worker.min.js")
        dest(extraResources.map { it.file("pdf.worker.mjs") })
        overwrite(false)
    }

    processResources {
        dependsOn(downloadPdfViewerCss, downloadPdfViewerJs, downloadPdfWorkerJs)
    }

    sourceSets.main {
        resources.srcDirs(extraResources)
    }

    // Set the JVM compatibility versions
    withType<JavaCompile> {
        sourceCompatibility = "21"
        targetCompatibility = "21"
    }
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        compilerOptions.jvmTarget = JvmTarget.JVM_21
    }

    patchPluginXml {
        sinceBuild = "243"
        untilBuild = "251.*"
        changeNotes = changelogHtml
    }

    signPlugin {
        certificateChain = System.getenv("CERTIFICATE_CHAIN")
        privateKey = System.getenv("PRIVATE_KEY")
        password = System.getenv("PRIVATE_KEY_PASSWORD")
    }

    publishPlugin {
        token = System.getenv("PUBLISH_TOKEN")
    }

    run {
        // workaround for https://youtrack.jetbrains.com/issue/IDEA-285839/Classpath-clash-when-using-coroutines-in-an-unbundled-IntelliJ-plugin
        buildPlugin {
            exclude { "coroutines" in it.name }
        }
        prepareSandbox {
            exclude { "coroutines" in it.name }
        }
    }
}
