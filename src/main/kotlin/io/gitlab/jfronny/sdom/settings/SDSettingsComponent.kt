package io.gitlab.jfronny.sdom.settings

import com.intellij.openapi.ui.ComboBox
import com.intellij.ui.dsl.builder.panel
import io.gitlab.jfronny.sdom.actions.SDLoginAction
import javax.swing.JPanel

class SDSettingsComponent {
    val mainPanel: JPanel
    val pdfModeBox: ComboBox<PdfMode> = ComboBox(PdfMode.entries.toTypedArray())

    init {
//        mainPanel = FormBuilder.createFormBuilder()
//            .addLabeledComponent("PDF Mode", pdfModeBox)
//            .addComponentFillVertically(JPanel(), 0)
//            .addComponentFillVertically(ActionManager.getInstance().createActionToolbar(
//                "SettingsUI",
//                DefaultActionGroup(SDLoginAction("Change Login Credentials")),
//                true
//            ).component, 0)
//            .panel
        mainPanel = panel {
            row {
                label("PDF Mode")
                cell(pdfModeBox)
            }
            row {
                button("Change Login Credentials") {
                    SDLoginAction.perform()
                }
            }
        }
    }
}