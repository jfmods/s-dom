package io.gitlab.jfronny.sdom.icons

import com.intellij.openapi.util.IconLoader

object SDIcons {
    @JvmField val ToolWindow = IconLoader.getIcon("/sd_plugin_logo.svg", javaClass)
}