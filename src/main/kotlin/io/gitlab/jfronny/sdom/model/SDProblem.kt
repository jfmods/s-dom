package io.gitlab.jfronny.sdom.model

import java.awt.Color

data class SDProblem(val id: String, val name: String, val rgb: Color, var solved: Boolean) : Comparable<SDProblem> {
    companion object {
        fun of(problem: Problem, solved: Boolean): SDProblem {
            return SDProblem(problem.id, problem.name, Color.decode(problem.rgb), solved)
        }
    }

    override fun compareTo(other: SDProblem): Int {
        return compareValuesBy(this, other, SDProblem::solved, { it.rgb.rgb }, { it.name.lowercase() }, SDProblem::id)
    }
}
