package io.gitlab.jfronny.sdom.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Contest(
    @SerialName("formal_name") val formalName: String,
    //not needed here @SerialName("scoreboard_type") val scoreboardType: String,
    @SerialName("start_time") val startTime: String, // DateTime
    @SerialName("end_time") val endTime: String, // DateTime
    //not needed here @SerialName("scoreboard_thaw_time") val scoreboardThawTime: String, // DateTime
    @SerialName("duration") val duration: String,
    @SerialName("scoreboard_freeze_duration") val scoreboardFreezeDuration: String?,
    //not needed here val banner: List<ImageFile>,
    //not needed here val problemset: List<FileWithName>,
    val id: String,
    @SerialName("external_id") val externalId: String?,
    val name: String,
    val shortname: String,
    @SerialName("allow_submit") val allowSubmit: Boolean,
    @SerialName("runtime_as_score_tiebreaker") val runtimeAsScoreTiebreaker: Boolean,
    @SerialName("warning_message") val warningMessage: String?,
    @SerialName("penalty_time") val penaltyTime: Int,
)