package io.gitlab.jfronny.sdom.model

import kotlinx.serialization.Serializable

@Serializable
data class FileWithName(
    val href: String,
    val mime: String,
    val filename: String,
)