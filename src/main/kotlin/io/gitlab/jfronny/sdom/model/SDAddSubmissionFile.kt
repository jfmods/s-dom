package io.gitlab.jfronny.sdom.model

import kotlinx.serialization.Serializable

@Serializable
data class SDAddSubmissionFile(
    val data: String,
    val mime: String? = "application/zip"
)
