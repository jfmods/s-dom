package io.gitlab.jfronny.sdom.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SDLoginResult(
    @SerialName("last_login_time") val lastLoginTime: String?, // DateTime
    @SerialName("last_api_login_time") val lastApiLoginTime: String?, // DateTime
    @SerialName("first_login_time") val firstLoginTime: String?, // DateTime
    val team: String?,
    @SerialName("team_id") val teamId: String?,
    val roles: List<String>,
    val type: String?,
    val id: String,
    val username: String,
    val name: String,
    val email: String?,
    @SerialName("last_ip") val lastIp: String?,
    val ip: String?,
    val enabled: Boolean
)
