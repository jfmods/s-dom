package io.gitlab.jfronny.sdom.model.scoreboard

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ScoreboardProblem(
    val label: String,
    @SerialName("problem_id") val problemId: String,
    @SerialName("num_judged") val numJudged: Int,
    @SerialName("num_pending") val numPending: Int,
    val solved: Boolean,
    //not needed here val time: Int,
    //not needed here @SerialName("first_to_solve") val firstToSolve: Boolean
)