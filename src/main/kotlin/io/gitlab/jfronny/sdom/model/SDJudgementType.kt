package io.gitlab.jfronny.sdom.model

import kotlinx.serialization.Serializable

@Serializable
data class SDJudgementType(
    val id: String,
    val name: String,
    val penalty: Boolean,
    val solved: Boolean
)
