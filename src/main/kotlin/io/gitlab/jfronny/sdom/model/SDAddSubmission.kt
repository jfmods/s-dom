package io.gitlab.jfronny.sdom.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SDAddSubmission(
    val problem: String?,
    @SerialName("problem_id") val problemId: String?,
    val language: String?,
    @SerialName("language_id") val languageId: String?,
    //@SerialName("team_id") val teamId: String? = null,
    //@SerialName("user_id") val userId: String? = null,
    //val time: String? = null, // DateTime
    @SerialName("entry_point") val entryPoint: String?,
    //val id: String? = null,
    val files: List<SDAddSubmissionFile>,
    //val code: List<String>?, // binary
)