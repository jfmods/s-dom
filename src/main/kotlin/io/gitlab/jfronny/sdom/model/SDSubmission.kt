package io.gitlab.jfronny.sdom.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SDSubmission(
    @SerialName("language_id") val languageId: String,
    val time: String,
    @SerialName("contest_time") val contestTime: String,
    @SerialName("team_id") val teamId: String,
    @SerialName("problem_id") val problemId: String,
    //not needed val files: List<FileWithName>?,
    val id: String,
    @SerialName("external_id") val externalId: String?,
    //not needed @SerialName("entry_point") val entryPoint: String?,
    //not needed, I guess? @SerialName("import_error") val importError: String?,
)