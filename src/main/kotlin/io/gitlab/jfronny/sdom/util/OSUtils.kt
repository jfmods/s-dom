package io.gitlab.jfronny.sdom.util

import java.awt.Desktop
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.regex.Pattern
import java.util.stream.Stream

object OSUtils {
    var TYPE: Type? = System.getProperty("os.name", "generic").lowercase().let { os ->
        if ((os.contains("mac")) || (os.contains("darwin"))) {
            Type.MAC_OS
        } else if (os.contains("win")) {
            Type.WINDOWS
        } else if (os.contains("nux")) {
            Type.LINUX
        } else {
            null // probably fine :)
        }
    }

    fun executablePathContains(executableName: String): Boolean {
        return try {
            Stream.of(
                *System.getenv("PATH").split(Pattern.quote(File.pathSeparator).toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray())
                .map { path: String -> path.replace("\"", "") }
                .map { first: String -> Paths.get(first) }
                .anyMatch { path: Path ->
                    (Files.exists(path.resolve(executableName))
                            && Files.isExecutable(path.resolve(executableName)))
                }
        } catch (e: Exception) {
            false
        }
    }

    fun openFile(path: File) {
        if (TYPE === Type.LINUX && executablePathContains("xdg-open")) {
            Runtime.getRuntime().exec(arrayOf("xdg-open", path.absolutePath))
        } else if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.OPEN)) {
            Desktop.getDesktop().open(path)
        }
    }

    enum class Type {
        WINDOWS, MAC_OS, LINUX
    }
}