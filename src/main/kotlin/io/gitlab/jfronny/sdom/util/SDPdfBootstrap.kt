package io.gitlab.jfronny.sdom.util

import com.intellij.openapi.editor.colors.ColorKey
import com.intellij.openapi.editor.colors.EditorColorsManager
import com.intellij.ui.JBColor
import com.intellij.ui.jcef.JBCefScrollbarsHelper
import com.intellij.util.ObjectUtils
import io.gitlab.jfronny.sdom.SDom
import org.intellij.lang.annotations.Language
import java.io.InputStreamReader
import java.util.*

private fun getCssColor(key: ColorKey): String {
    val colorScheme = EditorColorsManager.getInstance().schemeForCurrentUITheme
    val color = ObjectUtils.notNull(colorScheme.getColor(key), key.defaultColor)
    val alpha = color.alpha / 255.0
    return "rgba(${color.red}, ${color.green}, ${color.blue}, $alpha)"
}

private val backgroundColor = ColorKey.createColorKey("Editor.background", JBColor.WHITE)
private val viewerCss = SDom.javaClass.getResourceAsStream("/pdf.viewer.css")!!.use { InputStreamReader(it).use { it.readText() } }
private val pdfJs = SDom.javaClass.getResourceAsStream("/pdf.mjs")!!.use { InputStreamReader(it).use { it.readText() } }
private val pdfWorkerUrl = "data:text/plain;base64," + Base64.getEncoder().encodeToString(SDom.javaClass.getResourceAsStream("/pdf.worker.mjs")!!.use { it.readAllBytes() })
private const val scale = "1.5"

@Language("HTML")
fun pdfBootstrap(pdf: ByteArray) = """
    <!DOCTYPE html>
    <html>
    <head>
      <script type="module">
        $pdfJs
      </script>
    </head>
    <body>
    <script type="module">
      const pdfData = atob('${Base64.getEncoder().encodeToString(pdf)}');
    
      // Loaded via <script> tag, create shortcut to access PDF.js exports.
      const { pdfjsLib } = globalThis;
    
      // The workerSrc property shall be specified.
      pdfjsLib.GlobalWorkerOptions.workerSrc = '$pdfWorkerUrl';
    
      // Using DocumentInitParameters object to load binary data.
      const loadingTask = pdfjsLib.getDocument({data: pdfData});
      loadingTask.promise.then(function(pdf) {
        console.log('PDF loaded');
        
        const body = document.getElementById('our-body');
        for (let i = 1; i <= pdf.numPages; i++) {
          pdf.getPage(i).then(function(page) {
            console.log('Page loaded');
    
            const viewport = page.getViewport({scale: $scale});
    
            // Prepare canvas using PDF page dimensions
            const canvas = document.createElement('canvas');
            const textLayer = document.createElement('div');
            textLayer.className = "textLayer"
            body.appendChild(canvas)
            body.appendChild(textLayer)
            const context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;
    
            // Render PDF page into canvas context
            const renderTask = page.render({
              canvasContext: context,
              viewport: viewport
            });
            renderTask.promise.then(function () {
              return page.getTextContent()
            }).then(function (textContent) {
              pdfjsLib.renderTextLayer({
                textContentSource: textContent,
                container: textLayer,
                viewport: viewport,
                textDivs: []
              });
              textLayer.style.left = canvas.offsetLeft + 'px';
              textLayer.style.top = canvas.offsetTop + 'px';
              textLayer.style.height = canvas.offsetHeight + 'px';
              textLayer.style.width = canvas.offsetWidth + 'px';
            });
          });
        }
      }, function (reason) {
        // PDF loading error
        console.error(reason);
      });
    </script>
    <div id="our-body" style="width: 100%"></div>
    <style>
      $viewerCss
    </style>
    <style>
      ${JBCefScrollbarsHelper.getOverlayScrollbarsSourceCSS()}
      ${JBCefScrollbarsHelper.buildScrollbarsStyle()}
      body {
        background-color: ${getCssColor(backgroundColor)};
      }
      
      :root {
        --scale-factor: $scale;
      }
      
      /* for debugging:
      span {
        color: red !important;
        z-index: 99999;
        border: 1px solid red;
      }*/
    </style>
    <script>
      ${JBCefScrollbarsHelper.getOverlayScrollbarsSourceJS()}
    </script>
    </body>
    </html>
    """.trimIndent()