package io.gitlab.jfronny.sdom.util

import kotlin.reflect.KProperty

class MutableLazy<T>(val initializer: () -> T) {
    private object Uninitialized
    @Volatile private var value: Any? = Uninitialized

    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        val _v1 = value
        if (_v1 !== Uninitialized) {
            @Suppress("UNCHECKED_CAST")
            return _v1 as T
        }
        return synchronized(this) {
            val _v2 = value
            if (_v2 !== Uninitialized) {
                @Suppress("UNCHECKED_CAST")
                _v2 as T
            } else {
                val typedValue = initializer()
                value = typedValue
                typedValue
            }
        }
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        synchronized(this) {
            this.value = value
        }
    }
}