package io.gitlab.jfronny.sdom.actions

import com.intellij.notification.NotificationType
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.diagnostic.LogLevel
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.fileEditor.FileDocumentManager
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.vfs.readText
import io.gitlab.jfronny.sdom.SDom
import io.gitlab.jfronny.sdom.util.notify
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

class SDSubmitAction(text: String) : SDNotificationAction(text) {
    constructor() : this("")

    override fun update(e: AnActionEvent) {
        e.presentation.isVisible = SDom.loggedIn && e.project != null
        e.presentation.isEnabled = SDom.loggedIn && e.project != null && SDom.currentContest != null && SDom.currentProblem != null
    }

    override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT

    override fun perform(e: AnActionEvent) {
        e.project?.let { project ->
            val virtualFile = FileEditorManager.getInstance(project).selectedTextEditor?.virtualFile
            if (virtualFile == null) {
                notify(e.project,
                    "No file selected",
                    "You have to select a file",
                    NotificationType.ERROR,
                    SDSubmitAction("Retry")
                )
                return
            }
            FileDocumentManager.getInstance().run { getDocument(virtualFile)?.let { saveDocument(it) } }
            val currentFile = virtualFile.readText()
            val fileName = virtualFile.name
            val language = virtualFile.extension?.let { SDom.identifyLanguage(it) }

            if (language == null) {
                notify(e.project,
                    "Unknown file extension",
                    "Could not identify the language of that file",
                    NotificationType.ERROR
                )
                return
            }

            val contest = SDom.currentContest
            if (contest == null) {
                notify(e.project,
                    "No contest selected",
                    "You have to select a contest",
                    NotificationType.ERROR,
                    SDContestSelectionNotificationAction("Select Contest"),
                    SDSubmitAction("Retry")
                )
                return
            }
            val problem = SDom.currentProblem
            if (problem == null) {
                notify(e.project,
                    "No problem selected",
                    "You have to select a problem",
                    NotificationType.ERROR,
                    SDProblemSelectionNotificationAction("Select Problem"),
                    SDSubmitAction("Retry")
                )
                return
            }
            CoroutineScope(Job() + Dispatchers.IO).launch {
                val result = SDom.submitSolution(contest, problem, currentFile, fileName, language)
                LOG.info(result.first().toString())
            }
        }
    }

    companion object {
        private val LOG = Logger.getInstance(SDSubmitAction::class.java).apply {
            setLevel(LogLevel.DEBUG)
        }
    }
}