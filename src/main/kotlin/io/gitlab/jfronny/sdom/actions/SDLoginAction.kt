package io.gitlab.jfronny.sdom.actions

import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnActionEvent
import io.gitlab.jfronny.sdom.SDom
import io.gitlab.jfronny.sdom.ui.SDLoginDialogWrapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class SDLoginAction(text: String) : SDNotificationAction(text) {
    override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT

    constructor() : this("")

    override fun perform(e: AnActionEvent) {
        val dialogWrapper = SDLoginDialogWrapper()

        if (dialogWrapper.showAndGet()) {
            CoroutineScope(Job() + Dispatchers.IO).launch {
                SDom.login(username = dialogWrapper.username, password = dialogWrapper.password, url = dialogWrapper.url)
            }
        }
    }

    companion object {
        fun perform() = SDLoginAction().perform()
    }
}