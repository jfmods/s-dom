package io.gitlab.jfronny.sdom.actions

import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnActionEvent
import io.gitlab.jfronny.sdom.SDom
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class SDGetContestsAction : SDDumbAwareAction() {
    override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT

    override fun update(e: AnActionEvent) {
        e.presentation.isEnabledAndVisible = SDom.loggedIn
    }

    override fun perform(e: AnActionEvent) {
        CoroutineScope(Job() + Dispatchers.IO).launch {
            SDom.getContests()
            SDom.loadProperties(e.project)
        }
    }
}