package io.gitlab.jfronny.sdom.actions

import com.intellij.notification.Notification
import com.intellij.notification.NotificationAction
import com.intellij.openapi.actionSystem.*
import com.intellij.openapi.project.DumbAwareAction

fun nilActionEvent() = AnActionEvent(
    DataContext.EMPTY_CONTEXT,
    Presentation(),
    ActionPlaces.UNKNOWN,
    ActionUiKind.NONE,
    null,
    0,
    ActionManager.getInstance()
)

interface SDAction {
    fun perform(e: AnActionEvent)
    fun perform() {
        perform(nilActionEvent())
    }
}

abstract class SDNotificationAction(text: String) : NotificationAction(text), SDAction {
    override fun actionPerformed(e: AnActionEvent) = perform(e)
    override fun actionPerformed(e: AnActionEvent, notification: Notification) = perform(e)
}

abstract class SDDumbAwareAction : DumbAwareAction(), SDAction {
    override fun actionPerformed(e: AnActionEvent) = perform(e)
}
