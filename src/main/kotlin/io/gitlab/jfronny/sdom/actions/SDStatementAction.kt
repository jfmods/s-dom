package io.gitlab.jfronny.sdom.actions

import com.intellij.notification.NotificationType
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.openapi.vfs.VirtualFileManager
import com.intellij.ui.jcef.JBCefApp
import io.gitlab.jfronny.sdom.SDom
import io.gitlab.jfronny.sdom.SDom.currentContest
import io.gitlab.jfronny.sdom.SDom.currentProblem
import io.gitlab.jfronny.sdom.settings.PdfMode
import io.gitlab.jfronny.sdom.settings.SDSettings
import io.gitlab.jfronny.sdom.ui.ByteVirtualFile
import io.gitlab.jfronny.sdom.util.OSUtils
import io.gitlab.jfronny.sdom.util.notify
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardOpenOption

class SDStatementAction(name: String) : DumbAwareAction(name) {
    constructor() : this("")

    override fun update(e: AnActionEvent) {
        e.presentation.isVisible = SDom.loggedIn
        e.presentation.isEnabled = SDom.loggedIn && currentContest != null && currentProblem != null
    }

    override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT

    override fun actionPerformed(e: AnActionEvent) {
        val contest = currentContest
        val problem = currentProblem
        if (contest == null) {
            notify(e.project,
                "No contest selected",
                "You have to select a contest",
                NotificationType.ERROR,
                SDStatementAction("Retry")
            )
            return
        }
        if (problem == null) {
            notify(e.project,
                "No problem selected",
                "You have to select a problem",
                NotificationType.ERROR,
                SDStatementAction("Retry")
            )
            return
        }
        CoroutineScope(Job() + Dispatchers.IO).launch {
            val data = SDom.downloadProblemStatement(contest, problem)
            fun createTmpFile(): Path {
                val prefix = "${problem.name}_${contest.name}".replace("[^a-zA-Z0-9._-]".toRegex(), "_")
                val path = Files.createTempFile(prefix, ".pdf")
                Files.write(path, data, StandardOpenOption.WRITE)
                return path
            }
            fun open(virtualFile: VirtualFile) = ApplicationManager.getApplication().invokeLater {
                FileEditorManager.getInstance(e.project!!).openFile(virtualFile)
            }
            when (SDSettings.getInstance().state.pdfMode) {
                PdfMode.INTERNAL_MEMORY -> {
                    if (JBCefApp.isSupported()) open(ByteVirtualFile("${problem.name}_${contest.name}.pdf", data))
                    else OSUtils.openFile(createTmpFile().toFile())
                }
                PdfMode.INTERNAL_FILE -> {
                    if (JBCefApp.isSupported()) open(VirtualFileManager.getInstance().refreshAndFindFileByNioPath(createTmpFile())!!)
                    else OSUtils.openFile(createTmpFile().toFile())
                }
                PdfMode.EXTERNAL_FILE -> OSUtils.openFile(createTmpFile().toFile())
            }
        }
    }
}