package io.gitlab.jfronny.sdom.ui

import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.fileEditor.FileEditor
import com.intellij.openapi.fileEditor.FileEditorPolicy
import com.intellij.openapi.fileEditor.FileEditorProvider
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Disposer
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.ui.jcef.JBCefApp
import org.jetbrains.annotations.NonNls

class SDBrowserViewerProvider : FileEditorProvider, DumbAware {
    private val ourCefClient = JBCefApp.getInstance().createClient()

    init {
        Disposer.register(ApplicationManager.getApplication(), ourCefClient)
    }

    override fun accept(project: Project, virtualFile: VirtualFile): Boolean {
        return virtualFile.extension == "pdf"
    }

    override fun createEditor(project: Project, virtualFile: VirtualFile): FileEditor {
        return SDBrowserViewer(virtualFile, ourCefClient)
    }

    override fun getEditorTypeId(): @NonNls String {
        return EDITOR_TYPE_ID
    }

    override fun getPolicy(): FileEditorPolicy {
        return FileEditorPolicy.HIDE_DEFAULT_EDITOR
    }

    companion object {
        private const val EDITOR_TYPE_ID = "SdomBrowserViewer"
    }
}