package io.gitlab.jfronny.sdom.ui

import com.intellij.util.ui.components.BorderLayoutPanel
import java.awt.BorderLayout
import javax.swing.JComponent

class SDToolWindowPanel(
    private val topComponent: JComponent? = null,
    private val leftComplement: JComponent? = null,
    private val mainComponent: JComponent? = null,
    private val rightComplement: JComponent? = null
) {
    private val basePanel = BorderLayoutPanel(1, 1).apply {
        mainComponent?.let { add(it, BorderLayout.CENTER) }
        topComponent?.let { add(it, BorderLayout.NORTH) }
        leftComplement?.let { add(it, BorderLayout.WEST) }
        rightComplement?.let { add(it, BorderLayout.EAST) }
    }

    fun getPanel() = basePanel
}