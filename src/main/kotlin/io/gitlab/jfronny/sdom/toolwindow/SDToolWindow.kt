package io.gitlab.jfronny.sdom.toolwindow

import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.ActionPlaces
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.DataContext
import com.intellij.openapi.actionSystem.Presentation
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.dsl.builder.Align
import com.intellij.ui.dsl.builder.AlignY
import com.intellij.ui.dsl.builder.panel
import io.gitlab.jfronny.sdom.actions.SDLoginAction

fun loggedOutDialogPanel(): DialogPanel = panel {
    row {
        panel {
            row {
                label("You are not logged in. Please log in to use this tool.").align(Align.CENTER)
            }
            row {
                button("Log In") {
                    SDLoginAction.perform()
                }.align(Align.CENTER)
            }
        }.resizableColumn().align(AlignY.CENTER)
    }.resizableRow()
}