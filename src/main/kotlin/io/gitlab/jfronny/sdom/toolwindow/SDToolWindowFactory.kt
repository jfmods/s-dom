package io.gitlab.jfronny.sdom.toolwindow

import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Disposer
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory
import io.gitlab.jfronny.sdom.SDom
import io.gitlab.jfronny.sdom.model.Contest
import io.gitlab.jfronny.sdom.model.SDJudgement
import io.gitlab.jfronny.sdom.model.SDProblem
import io.gitlab.jfronny.sdom.settings.SDCredentials
import io.gitlab.jfronny.sdom.ui.SDSubmitPanel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class SDToolWindowFactory : ToolWindowFactory, DumbAware {
    override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
        val contentManager = toolWindow.contentManager
        val loggedOutContent = contentManager.factory.createContent(loggedOutDialogPanel(), null, false).apply {
            isCloseable = false
        }

        val submitPanel = SDSubmitPanel(project)
        val submitContent = contentManager.factory.createContent(submitPanel.component, "Submit", false).apply {
            isCloseable = false
        }
        Disposer.register(toolWindow.disposable, submitPanel)

        fun showSubmitContent() {
            contentManager.addContent(submitContent)
            contentManager.removeContent(loggedOutContent, true)
        }

        fun showLoggedOutContent() {
            contentManager.removeContent(submitContent, true)
            contentManager.addContent(loggedOutContent)
        }

        fun showResultContent(resultFlow: Flow<Result<SDJudgement>>, contest: Contest, problem: SDProblem, filename: String) {
            submitPanel.logSubmission(contest, problem, filename)

            CoroutineScope(Job() + Dispatchers.IO).launch {
                resultFlow.collect { result ->
                    submitPanel.logSDJudgement(result, contest, problem, filename)
                }
            }
        }

        SDCredentials.loggedIn // trigger lazy initialization
        SDom.registerLoginListener(::showSubmitContent)
        SDom.registerLogoutListener(::showLoggedOutContent)
        SDom.registerResultFlowListener(::showResultContent)

        if (SDom.loggedIn) {
            showSubmitContent()
        } else {
            contentManager.addContent(loggedOutContent)
        }
    }
}